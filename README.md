# A tiny FIX message parser

FIX is a network protocol widely used in the finance industry. Your task is to implement a parser that handles a sequence of the ​Market Data Full Refresh​ message.

## Usage

```bash
java -jar just-fix-parser-0.0.1-jar-with-dependencies.jar <path_to_file>
```

where `path_to_file` is the path where FIX data is recorded

## Build

```bash
mvn clean package
```