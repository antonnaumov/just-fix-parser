package org.bitbucket.antonnaumov.justfixparser.model;

/**
 * MDFR currency operation types.
 */
public enum OperationType {
    BUY, SELL;

    public static OperationType parseString(final String operation) {
        return operation.equals("0") ? BUY : SELL;
    }
}
