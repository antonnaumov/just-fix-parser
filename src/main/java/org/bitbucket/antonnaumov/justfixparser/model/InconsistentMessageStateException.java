package org.bitbucket.antonnaumov.justfixparser.model;

/**
 * An exception identifies an attempt to build an {@link Message} instance based on inconsistent data.
 */
public class InconsistentMessageStateException extends Exception {
    /**
     * Construct the exception instance identifies the missing field as an input.
     *
     * @param field the field name has no value in the data set.
     */
    public InconsistentMessageStateException(final String field) {
        super("The field value [" + field + "] is missing to create a Message object");
    }

    /**
     * Construct the exception instance base on inconsistency cause.
     *
     * @param cause the {@link Exception} leads to the inconsistency.
     */
    public InconsistentMessageStateException(final Exception cause) {
        super(cause);
    }
}
