package org.bitbucket.antonnaumov.justfixparser.model;

/**
 * An exception identifies MDFR message check sum validation failure.
 */
public class InvalidMessageCheckSumException extends Exception {
    /**
     * Construct the exception instance using different check sum arguments as an input.
     *
     * @param actualCheckSum the MDFR message check sum calculated based on the data source.
     * @param expectedCheckSum the MDFR message check sum send in the scope of MDFR message.
     */
    public InvalidMessageCheckSumException(final int actualCheckSum, final int expectedCheckSum) {
        super("The message actual check sum [" + actualCheckSum + "] is not equals with expected one [" + expectedCheckSum + "]");
    }
}
