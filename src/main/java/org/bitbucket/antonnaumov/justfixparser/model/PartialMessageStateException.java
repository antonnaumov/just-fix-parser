package org.bitbucket.antonnaumov.justfixparser.model;

/**
 * An exception identifies an attempt to build an {@link Message} instance based on partial data.
 */
public class PartialMessageStateException extends Exception {
    /**
     * Construct the exception instance identifies the missing field as an input.
     *
     * @param field the field name has no value in the data set.
     */
    PartialMessageStateException(final String field) {
        super("The field value [" + field + "] is missing to create a Message object");
    }
}
