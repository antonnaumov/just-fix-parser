package org.bitbucket.antonnaumov.justfixparser.model;

import java.math.BigDecimal;

/**
 * An MDFR currency operation representation.
 *
 * <p>The particular operation is divided to {@link OperationType#BUY} and {@link OperationType#SELL} types. Each
 * operation contains the exchange price and source currency amount.</p>
 */
public class CurrencyOperation {
    private final OperationType operationType;
    private final BigDecimal price;
    private final BigDecimal amount;

    CurrencyOperation(final OperationType operationType) {
        this.operationType = operationType;
        price = BigDecimal.valueOf(0L);
        amount = BigDecimal.valueOf(0L);
    }

    public CurrencyOperation(final OperationType operationType, final BigDecimal price, final BigDecimal amount) {
        this.operationType = operationType;
        this.price = price;
        this.amount = amount;
    }

    public OperationType getOperationType() {
        return operationType;
    }

    public BigDecimal getPrice() {
        return price;
    }

    public BigDecimal getAmount() {
        return amount;
    }
}
