package org.bitbucket.antonnaumov.justfixparser.model;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

/**
 * MDFR message representation.
 *
 * <p>The message contains the currency pair symbol, the common timestamp and the list of {@link CurrencyOperation} objects</p>
 */
public class Message {
    private final String currencyPair;
    private final LocalDateTime timestamp;
    private final List<CurrencyOperation> operations = new ArrayList<>();

    public Message(final String currencyPair,
                   final LocalDateTime timestamp,
                   final List<CurrencyOperation> operations) throws InconsistentMessageStateException {
        this.currencyPair = Optional.ofNullable(currencyPair).orElseThrow(() -> new InconsistentMessageStateException("currencyPair"));
        this.timestamp = Optional.ofNullable(timestamp).orElseThrow(() -> new InconsistentMessageStateException("timestamp"));
        Optional.ofNullable(operations).ifPresent(this.operations::addAll);
    }

    public String getCurrencyPair() {
        return currencyPair;
    }

    public LocalDateTime getTimestamp() {
        return timestamp;
    }

    public List<CurrencyOperation> getOperations() {
        return operations;
    }

    public CurrencyOperation findFirstOperation(final OperationType operationType) {
        return operations
                .stream()
                .filter(op -> op.getOperationType() == operationType)
                .findFirst()
                .orElse(new CurrencyOperation(operationType));
    }
}
