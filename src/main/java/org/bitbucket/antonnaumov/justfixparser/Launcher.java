package org.bitbucket.antonnaumov.justfixparser;

import org.bitbucket.antonnaumov.justfixparser.processor.ConsoleOutputMessageProcessor;
import picocli.CommandLine;

import java.io.File;
import java.io.FileReader;
import java.util.concurrent.Callable;

@CommandLine.Command(description = "Parse Market Data Full Refresh messages from the input file", name = "MDF message parser")
public class Launcher implements Callable<Void> {
    @CommandLine.Parameters(index = "0", description = "The file contains various Market Data Full Refresh messages")
    private File source;

    private final ConsoleOutputMessageProcessor processor = new ConsoleOutputMessageProcessor();
    private final Parser parser = new Parser(processor);

    public static void main(final String[] args) {
        CommandLine.call(new Launcher(), System.err, args);
    }

    public Void call() throws Exception {
        processor.printHeader();
        parser.parse(new FileReader(source));
        processor.printFooter();
        return null;
    }
}
