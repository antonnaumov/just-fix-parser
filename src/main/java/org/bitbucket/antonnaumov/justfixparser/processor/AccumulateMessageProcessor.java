package org.bitbucket.antonnaumov.justfixparser.processor;

import org.bitbucket.antonnaumov.justfixparser.MessageProcessor;
import org.bitbucket.antonnaumov.justfixparser.model.Message;

import java.util.ArrayList;
import java.util.List;

/**
 * The {@link MessageProcessor} implementation accumulates accepted and rejected MDFR message statistic together with
 * the list of the valid {@link Message} messages in memory.
 */
public class AccumulateMessageProcessor implements MessageProcessor {
    private int rejectedMessageCount = 0;
    private final List<Message> messages = new ArrayList<>();

    /**
     * Add the message to the internal messages list.
     *
     * @param message the {@link org.bitbucket.antonnaumov.justfixparser.model.Message} instance represents valid
     */
    @Override
    public void acceptMessage(final Message message) {
        messages.add(message);
    }

    /**
     * Increase the rejected messages counter.
     *
     * @param reason the {@link java.lang.Exception} as the rejection reason.
     */
    @Override
    public void rejectMessage(final Exception reason) {
        rejectedMessageCount++;
    }

    /**
     * Cleanup the messages statistic if any.
     */
    public void reset() {
        rejectedMessageCount = 0;
        messages.clear();
    }

    public int getAcceptedMessageCount() {
        return messages.size();
    }

    public int getRejectedMessageCount() {
        return rejectedMessageCount;
    }

    public List<Message> getMessages() {
        return messages;
    }
}
