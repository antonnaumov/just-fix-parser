package org.bitbucket.antonnaumov.justfixparser.processor;

import org.bitbucket.antonnaumov.justfixparser.MessageProcessor;
import org.bitbucket.antonnaumov.justfixparser.model.CurrencyOperation;
import org.bitbucket.antonnaumov.justfixparser.model.Message;
import org.bitbucket.antonnaumov.justfixparser.model.OperationType;

import java.time.format.DateTimeFormatter;

/**
 * The {@link MessageProcessor} implementation prints MDFR messages together with accepted/rejected statistic to console.
 *
 * <p>The output is build as an ASCII table consist of the static header, the footer display accumulated
 * accepted/rejected statistic and the rows displays valid messages information.</p>
 *
 * <p>For example:
 * <blockquote><pre>{@code
 * +--------+-----------+------------+------------+-------------+-------------------------+
 * | Symbol | Buy Price | Sell Price | Buy Amount | Sell Amount |        Timestamp        |
 * +--------+-----------+------------+------------+-------------+-------------------------+
 * | EURUSD |  1,31678  |   1,31678  | 100.000,00 | 100.000,00  | 06/02/2018 21:43:36.000 |
 * | EURNOK |  1,75635  |   1,75635  | 100.000,00 | 100.000,00  | 06/02/2018 21:43:36.020 |
 * +--------+-----------+------------+------------+-------------+-------------------------+
 * | Total: | Rejected: 10  , Accepted: 0                                                 |
 * +--------+-----------+------------+------------+-------------+-------------------------+
 * }</pre></blockquote>
 * </p>
 */
public class ConsoleOutputMessageProcessor implements MessageProcessor {
    private static final DateTimeFormatter DATE_TIME_FORMATTER = DateTimeFormatter.ofPattern("dd/MM/yyyy HH:mm:ss.SSS");
    private static final String SPLITTER_TEMPLATE =
            "+--------+-----------+------------+------------+-------------+-------------------------+%n";
    private static final String HEADER_TEMPLATE =
            "| Symbol | Buy Price | Sell Price | Buy Amount | Sell Amount |        Timestamp        |%n";
    private static final String ROW_TEMPLATE =
            "| %6s |  %1.5f  |   %1.5f  | %(,.2f | %(,.2f  | %21s |%n";
    private static final String FOOTER_TEMPLATE =
            "| Total: | Rejected: %-4d, Accepted: %-4d                                              |%n";


    private int acceptedMessageCount = 0;
    private int rejectedMessageCount = 0;

    /**
     * Prints out the table contains the currency pair symbol, the price to buy, the amount to buy, the price to sell,
     * the amount to sell and the message timestamp. Increase the accepted message counter.
     *
     * @param message the {@link org.bitbucket.antonnaumov.justfixparser.model.Message} instance represents valid
     */
    @Override
    public void acceptMessage(final Message message) {
        acceptedMessageCount++;
        final CurrencyOperation buyOp = message.findFirstOperation(OperationType.BUY);
        final CurrencyOperation sellOp = message.findFirstOperation(OperationType.SELL);
        System.out.format(
                ROW_TEMPLATE,
                message.getCurrencyPair(),
                buyOp.getPrice().doubleValue(),
                buyOp.getPrice().doubleValue(),
                sellOp.getAmount().doubleValue(),
                sellOp.getAmount().doubleValue(),
                DATE_TIME_FORMATTER.format(message.getTimestamp())
        );
    }

    /**
     * Increase the rejected message counter.
     *
     * @param reason the {@link java.lang.Exception} as the rejection reason.
     */
    @Override
    public void rejectMessage(final Exception reason) {
        rejectedMessageCount++;
    }

    /**
     * Prints out the ASCII table header.
     */
    public void printHeader() {
        System.out.format(SPLITTER_TEMPLATE);
        System.out.format(HEADER_TEMPLATE);
        System.out.format(SPLITTER_TEMPLATE);
    }

    /**
     * Prints out the ASCII table footer.
     */
    public void printFooter() {
        System.out.format(SPLITTER_TEMPLATE);
        System.out.format(FOOTER_TEMPLATE, acceptedMessageCount, rejectedMessageCount);
        System.out.format(SPLITTER_TEMPLATE);
    }
}
