package org.bitbucket.antonnaumov.justfixparser;

import java.util.Scanner;
import java.util.regex.Pattern;

/**
 * A MDFR messages parser.
 *
 * <p>A <code>Parser</code> treats an input as an infinite stream of <code>key=value</code> pairs separated by the
 * ASCII character SOH (start-of-heading) symbol.</p>
 *
 * <p>The parser is using a {@link org.bitbucket.antonnaumov.justfixparser.MessageBuilder} to construct a
 * {@link org.bitbucket.antonnaumov.justfixparser.model.Message} from each valid data sequence.</p>
 *
 * <p>The parser is notify a {@link org.bitbucket.antonnaumov.justfixparser.MessageProcessor} instance with each
 * accepted and rejected message during the parsing process.</p>
 */
class Parser {
    private static final Pattern MESSAGE_DELIMITER = Pattern.compile("\u0001");
    private static final String KEY_VALUE_DELIMITER = "=";

    private final MessageProcessor processor;
    private final MessageBuilder builder = new MessageBuilder();


    /**
     * Creates the parser instance engaged with the {@link org.bitbucket.antonnaumov.justfixparser.MessageProcessor}
     * implementation.
     *
     * @param processor the {@link org.bitbucket.antonnaumov.justfixparser.MessageProcessor} implementation to notify
     *                  with an accepted and rejected message.
     */
    Parser(final MessageProcessor processor) {
        this.processor = processor;
    }

    /**
     * Parsing the {@link java.lang.Readable} instance to collect MDFR messages.
     *
     * <p>The parser implementation is react to MDFR keys as following:
     * <blockquote><pre>{@code
     * 8 Beginning Symbol   - start a message recoding
     * 10 CheckSum          - try to build the message after validation is complete
     * 52 Timestamp         - record the message timestamp
     * 55 Symbol            - record the message currency pair
     * 268 NumEntries       - record the number of prices for this currency pair
     * 269 BuyOrSell        - record the message operation type
     * 270 Price            - record the message operation price
     * 271 Amount           - record the message operation amount
     * }</pre></blockquote>
     * </p>
     *
     * @param source the {@link java.lang.Readable} instance consist of MDFR messages.
     */
    void parse(final Readable source) {
        final Scanner scanner = new Scanner(source).useDelimiter(MESSAGE_DELIMITER);
        while(scanner.hasNext()) {
            final String entry = scanner.next();
            parseEntry(entry, builder);
        }
    }

    private void parseEntry(final String entry, final MessageBuilder builder) {
        try {
            final String[] pair = entry.split(KEY_VALUE_DELIMITER);
            final int key = Integer.valueOf(pair[0]);
            final String value = pair[1];
            switch(key) {
                case 8:
                    builder.init().checkSumPair(entry);
                    break;
                case 10:
                    processor.acceptMessage(builder.build(calculateCheckSum(value)));
                    break;
                case 52:
                    builder.withTimestamp(value).checkSumPair(entry);
                    break;
                case 55:
                    builder.withCurrencySymbol(value).checkSumPair(entry);
                    break;
                case 268:
                    builder.withOperationsCount(value).checkSumPair(entry);
                    break;
                case 269:
                    builder.withOperation(value).checkSumPair(entry);
                    break;
                case 270:
                    builder.withOperationPrice(value).checkSumPair(entry);
                    break;
                case 271:
                    builder.withOperationAmount(value).checkSumPair(entry);
                    break;
                default:
                    builder.checkSumPair(entry);
            }
        } catch(Exception e) {
            if (!builder.isRejected()) {
                processor.rejectMessage(e);
                builder.recordRejectionReason(e);
            }
        }
    }

    private int calculateCheckSum(final String value) {
        try {
            return Integer.parseInt(value);
        } catch(NumberFormatException e) {
            return 0;
        }
    }
}
