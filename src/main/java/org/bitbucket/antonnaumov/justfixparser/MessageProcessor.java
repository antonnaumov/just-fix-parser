package org.bitbucket.antonnaumov.justfixparser;

import org.bitbucket.antonnaumov.justfixparser.model.Message;

/**
 * An interface of reactive message processor.
 *
 * <p>The processor expose the methods reacts to valid {@link #acceptMessage} and invalid {@link #rejectMessage}
 * MDFR message finding.</p>
 */
public interface MessageProcessor {
    /**
     * Process the MDFR message.
     *
     * @param message the {@link org.bitbucket.antonnaumov.justfixparser.model.Message} instance represents valid
     *                MDFR message.
     */
    void acceptMessage(final Message message);

    /**
     * Process the MDFR message rejection reason.
     *
     * <p>The invalid MDFR message means {@link org.bitbucket.antonnaumov.justfixparser.model.Message} instance
     * creation is impossible.</p>
     *
     * @param reason the {@link java.lang.Exception} as the rejection reason.
     */
    void rejectMessage(final Exception reason);
}
