package org.bitbucket.antonnaumov.justfixparser;

import org.bitbucket.antonnaumov.justfixparser.model.*;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

/**
 * The standard Builder pattern implementation, to compose an {@link Message} instance from MDFR message record if
 * possible.
 */
class MessageBuilder {
    static final DateTimeFormatter DATE_TIME_FORMATTER = DateTimeFormatter.ofPattern("yyyyMMdd-HH:mm:ss.SSS");
    private static final int CHECK_SUM_DIVIDER = 256;

    private final List<CurrencyOperation> operations = new ArrayList<>();
    private String checkSumSource;
    private String currencyPair;
    private LocalDateTime timestamp;
    private OperationType operationType;
    private BigDecimal operationPrice;
    private BigDecimal operationAmount;
    private int operationsCount = 0;
    private Exception rejectionReason;

    MessageBuilder() {}

    /**
     * Initiates the message recoding, cleanup all partial information recorded previously.
     *
     * @return {@link MessageBuilder} instance.
     */
    MessageBuilder init() {
        reset();
        return this;
    }

    /**
     * Records currency symbol.
     *
     * @param symbol the currency symbol.
     * @return {@link MessageBuilder} instance.
     */
    MessageBuilder withCurrencySymbol(final String symbol) {
        this.currencyPair = symbol;
        return this;
    }

    /**
     * Records the message timestamp as {@link java.time.LocalDateTime}.
     *
     * @param timestamp the string represents the message timestamp corresponds to {@code yyyyMMdd-HH:mm:ss.SSS} pattern.
     * @return {@link MessageBuilder} instance.
     */
    MessageBuilder withTimestamp(final String timestamp) {
        this.timestamp = LocalDateTime.parse(timestamp, DATE_TIME_FORMATTER);
        return this;
    }

    /**
     * Records operation type symbol.
     *
     * <p>As the side effect of the operation recoding, the method is trying to instantiate an {@link CurrencyOperation}
     * object. As soon as the object is created, it settled to the operations collections and {@code operationPrice},
     * {@code operationAmount} and {@code operationType} fields reset to initial state.</p>

     * @param operation the string encode operation type in numeric
     *                  format: 0 - {@link OperationType#BUY}, 1 - {@link OperationType#SELL}.
     * @return {@link MessageBuilder} instance.
     */
    MessageBuilder withOperation(final String operation) {
        operationType = OperationType.parseString(operation);
        composeOperation();
        return this;
    }

    /**
     * Records operation price value.
     *
     * <p>As the side effect of the price recoding, the method is trying to instantiate an {@link CurrencyOperation}
     * object. As soon as the object is created, it settled to the operations collections and {@code operationPrice},
     * {@code operationAmount} and {@code operationType} fields are reset to initial state.</p>
     *
     * @param price the string encode operation price as floating point correspond to {@code #.#####} pattern.
     * @return {@link MessageBuilder} instance.
     */
    MessageBuilder withOperationPrice(final String price) {
        operationPrice = BigDecimal.valueOf(Double.parseDouble(price));
        composeOperation();
        return this;
    }

    /**
     * Records operation amount value.
     *
     * <p>As the side effect of the amount recoding, the method is trying to instantiate an {@link CurrencyOperation}
     * object. As soon as the object is created, it settled to the operations collections and {@code operationPrice},
     * {@code operationAmount} and {@code operationType} fields are reset to initial state.</p>

     * @param amount the string encode operation price as floating point correspond to {@code ######.##} pattern.
     * @return {@link MessageBuilder} instance.
     */
    MessageBuilder withOperationAmount(final String amount) {
        operationAmount = BigDecimal.valueOf(Double.parseDouble(amount));
        composeOperation();
        return this;
    }

    /**
     * Records operations count value.
     *
     * @param count the string encode operations count the MDFR message represents.
     * @return {@link MessageBuilder} instance.
     */
    MessageBuilder withOperationsCount(final String count) {
        operationsCount = Integer.parseInt(count);
        return this;
    }

    /**
     * Records the raw {@code key=value} pair ended up with ASCII character SOH (start-of-heading) symbol as the source
     * to check sum calculation.
     *
     * @param rawPair the string represents raw {@code key=value} parsed out from MDFR message.
     * @return {@link MessageBuilder} instance.
     */
    MessageBuilder checkSumPair(final String rawPair) {
        if (Optional.ofNullable(checkSumSource).isPresent()) {
            checkSumSource += rawPair + "\u0001";
        }
        return this;
    }

    /**
     * Builds {@link Message} based on the recorded information.
     *
     * <p>Before compose the message, the method is performs following validations:
     * <ol>
     *     <li>Check if any inconsistency recoded during the build</li>
     *     <li>Check if recorded operation count presents in the message</li>
     *     <li>Compose the message check sum based on recorded raw {@code key=value} pairs and compare with the
     *     checkSum parameter</li>
     *     <li>Check if the currencyPair and timestamp values are present</li>
     * </ol>
     * </p>
     *
     * @param checkSum the check sum parsed out from MDFR message.
     * @return {@link MessageBuilder} instance.
     * @throws InvalidMessageCheckSumException if the MDFR message check sum validation failed
     * @throws InconsistentMessageStateException if the MDFR message content is inconsistent to instantiate a {@link Message}
     */
    Message build(final int checkSum) throws InvalidMessageCheckSumException, InconsistentMessageStateException {
        if (isRejected()) {
            throw new InconsistentMessageStateException(rejectionReason);
        }
        if(operations.size() != operationsCount) {
            throw new InconsistentMessageStateException("operations");
        }
        validateCheckSum(checkSum);
        return new Message(currencyPair, timestamp, operations);
    }

    /**
     * Indicates is the message once mark as rejected during the recoding.
     *
     * @return {@code true} if message was mark as rejected and {@code false} otherwise.
     */
    boolean isRejected() {
        return Optional.ofNullable(rejectionReason).isPresent();
    }

    /**
     * Records the rejection reason during the message building.
     *
     * @param reason an {@link Exception} leads to message rejection.
     */
    void recordRejectionReason(final Exception reason) {
        rejectionReason = reason;
    }

    private void validateCheckSum(final int checkSum) throws InvalidMessageCheckSumException {
        final int actualCheckSum = calculateCheckSum();
        if (actualCheckSum != checkSum) {
            throw new InvalidMessageCheckSumException(actualCheckSum, checkSum);
        }
    }

    private int calculateCheckSum() {
        int checkSum = 0;
        for (byte b: Optional.ofNullable(checkSumSource).orElse("").getBytes()) {
            checkSum += b;
        }
        return checkSum % CHECK_SUM_DIVIDER;
    }

    private void reset() {
        checkSumSource = "";
        currencyPair = null;
        timestamp = null;
        rejectionReason = null;
        operationsCount = 0;
        operations.clear();
        resetOperation();
    }

    private void resetOperation() {
        operationType = null;
        operationPrice = null;
    }

    private void composeOperation() {
        if (Optional.ofNullable(operationType).isPresent() &&
                Optional.ofNullable(operationPrice).isPresent() &&
                Optional.ofNullable(operationAmount).isPresent()) {
            operations.add(new CurrencyOperation(operationType, operationPrice, operationAmount));
            resetOperation();
        }
    }
}
