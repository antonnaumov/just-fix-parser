package org.bitbucket.antonnaumov.justfixparser;

import org.bitbucket.antonnaumov.justfixparser.model.CurrencyOperation;
import org.bitbucket.antonnaumov.justfixparser.model.Message;
import org.bitbucket.antonnaumov.justfixparser.model.OperationType;
import org.bitbucket.antonnaumov.justfixparser.processor.AccumulateMessageProcessor;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.StringReader;
import java.math.BigDecimal;
import java.time.LocalDateTime;

public class ParserTest {
    private final AccumulateMessageProcessor processor = new AccumulateMessageProcessor();
    private final Parser parser = new Parser(processor);

    @Test
    void parseCorrectMessage() {
        final String data = "8=FIX.4.4\u00019=142\u000135=W\u000134=0\u000149=justtech\u000152=20180206-21:43:36.000\u000156=user\u0001262=TEST\u000155=EURUSD\u0001268=2\u0001269=0\u0001270=1.31678\u0001271=100000.0\u0001269=1\u0001270=1.31667\u0001271=100000.0\u000110=057\u0001";
        parser.parse(new StringReader(data));
        Assertions.assertEquals(1, processor.getAcceptedMessageCount());
        Assertions.assertEquals(0, processor.getRejectedMessageCount());
        final Message message = processor.getMessages().get(0);
        Assertions.assertEquals("EURUSD", message.getCurrencyPair());
        Assertions.assertEquals(LocalDateTime.parse("20180206-21:43:36.000", MessageBuilder.DATE_TIME_FORMATTER), message.getTimestamp());
        Assertions.assertEquals(2, message.getOperations().size());
        CurrencyOperation op = message.getOperations().get(0);
        Assertions.assertSame(OperationType.BUY, op.getOperationType());
        Assertions.assertEquals(BigDecimal.valueOf(1.31678D), op.getPrice());
        Assertions.assertEquals(BigDecimal.valueOf(100000.0D), op.getAmount());
        op = message.getOperations().get(1);
        Assertions.assertSame(OperationType.SELL, op.getOperationType());
        Assertions.assertEquals(BigDecimal.valueOf(1.31667D), op.getPrice());
        Assertions.assertEquals(BigDecimal.valueOf(100000.0D), op.getAmount());
    }

    @Test
    void parseInvalidCheckSumMessage() {
        final String data = "8=FIX.4.4\u00019=142\u000135=W\u000134=0\u000149=justtech\u000152=20180206-21:43:36.000\u000156=user\u0001262=TEST\u000155=EURUSD\u0001268=2\u0001269=0\u0001270=1.31678\u0001271=100000.0\u0001269=1\u0001270=1.31667\u0001271=100000.0\u000110=055\u0001";
        parser.parse(new StringReader(data));
        Assertions.assertEquals(0, processor.getAcceptedMessageCount());
        Assertions.assertEquals(1, processor.getRejectedMessageCount());
    }

    @Test
    void parseInvalidOperationsMessage() {
        final String data = "8=FIX.4.4\u00019=142\u000135=W\u000134=0\u000149=justtech\u000152=20180206-21:43:36.000\u000156=user\u0001262=TEST\u000155=EURUSD\u0001268=2\u0001269=0\u0001270=1.31678\u0001271=100000.0\u000110=055\u0001";
        parser.parse(new StringReader(data));
        Assertions.assertEquals(0, processor.getAcceptedMessageCount());
        Assertions.assertEquals(1, processor.getRejectedMessageCount());
    }

    @Test
    void parseSemanticallyCorrectButNotFulfilMessage() {
        final String data = "8=FIX.4.1\u00019=61\u000135=A\u000134=1\u000152=20000426-12:05:06\u000198=0\u0001108=30\u000110=157\u0001";
        parser.parse(new StringReader(data));
        Assertions.assertEquals(0, processor.getAcceptedMessageCount());
        Assertions.assertEquals(1, processor.getRejectedMessageCount());
    }

    @Test
    void parseBatchEntities() {
        final InputStream is = Thread.currentThread().getContextClassLoader().getResourceAsStream("example-fix-data.bin");
        parser.parse(new InputStreamReader(is));
        Assertions.assertEquals(10, processor.getAcceptedMessageCount());
        Assertions.assertEquals(0, processor.getRejectedMessageCount());
    }

    @BeforeEach
    void cleanup() {
        processor.reset();
    }
}
